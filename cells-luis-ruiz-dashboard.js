{
  const {
    html,
  } = Polymer;
  /**
    `<cells-luis-ruiz-dashboard>` Description.

    Example:

    ```html
    <cells-luis-ruiz-dashboard></cells-luis-ruiz-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-luis-ruiz-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLuisRuizDashboard extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-luis-ruiz-dashboard';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-luis-ruiz-dashboard-styles cells-luis-ruiz-dashboard-shared-styles"></style>
      <slot></slot>
          <p><img src="../leon.jpeg"></p>
          <p>Nombre: Luis Enrique </p>
          <p>Apellidos: Ruiz Ruiz</p>
          <p>Dirección: Calle Adios #274</p>
          <p>Hobbies: Leer, jugar video juegos</p>
      `;
    }
  }

  customElements.define(CellsLuisRuizDashboard.is, CellsLuisRuizDashboard);
}